/*
 * Written by Redhart (ancientzero@tuta.io)
 * 2016-05-09 21:14
*/

package quadratic.solutions;

class QuadSolC {

    private static double a; 
    private static double b;
    private static double c;
    private static double discriminant;
    private static double[] r = new double[2];
    private static double real;
    private static double imag;
    private static String[] X = new String[2];
    private static String polynomial;
    
    public QuadSolC(int A, int B, int C)
    {
        a = A;
        b = B;
        c = C;
        polynomial = ((int)a + "x^2 " + (b < 0 ? "-":"+") + " "
                + Math.abs((int)b)+"x " + (c < 0 ? "-":"+")
                + " " + Math.abs((int)c) + " = 0");
        getSol();
    }
    
    public static void getSol()
    {
        discriminant = Math.pow(b, 2) - 4 * a * c;
		  
		  if (discriminant > 0)
		  {
			  // real roots
		      r[0] = (-b + Math.sqrt(discriminant)) / (2 * a);
		      r[1] = (-b - Math.sqrt(discriminant)) / (2 * a);
              X[0] = ""+r[0];
              X[1] = ""+r[1];
		  }
		  else if (discriminant == 0)
		  {
			  // equal roots
			  r[0] = r[1] = -b / (2 * a);
              X[0] = ""+r[0];
              X[1] = ""+r[1];
		  }
		  else
		  {
			  // complex roots
			  real = -b / (2 * a);
			  imag = Math.sqrt(-discriminant) / (2 * a);
			  X[0] = String.format("%.2f + %.2fi", real, imag);
			  X[1] = String.format("%.2f - %.2fi", real, imag);
		  }
    }
    
    public static double[] getR() { return r; }
    public static String[] getX() { return X; }    
    public static String getPolynomial() { return polynomial; }    
}
